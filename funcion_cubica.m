%% GRAFICA DE LA FUNCI�N C�BICA MEDIANTE UNA RNA
% *************************************************************************
close all
clear all
clc
% Definimos las entradas y salidas deseadas para el entrenamiento y la
% validaci�n
entradas = -5:1:5;
salidas = entradas.^3;
entradas_v = -5:0.1:5;
salidas_v = entradas_v.^3;
epoch_max = 10000;
capas_ocultas_max = 100;
for capas_oculta = 1:10:capas_ocultas_max
%% Configuramos los par�metros de la red y graficamos

cubica = newff(minmax(entradas),[capas_oculta 1],{'tansig' 'purelin'});
cubica.trainParam.epochs = 5000;
cubica.trainParam.goal = 0.0001;
[cubica,tr] = train(cubica,entradas,salidas);
tr.best_perf
Y = sim(cubica,entradas);
V = sim(cubica,entradas_v);

jframe = view(cubica);
%# create it in a MATLAB figure
hFig = figure('Menubar','none', 'Position',[100 100 565 166]);
jpanel = get(jframe,'ContentPane');
[~,h] = javacomponent(jpanel);
set(h, 'units','normalized', 'position',[0 0 1 1])

%# close java window
jframe.setVisible(false);
jframe.dispose();

%# print to file
set(hFig, 'PaperPositionMode', 'auto')
saveas(hFig, ['red_', num2str(capas_oculta),'_capas.png'])

%# close figure
close(hFig)

figure(1)
plot(entradas,salidas,'g');
grid on;
hold on;
plot(entradas,Y,'ro');
hold on;
plot(entradas_v,V,'bx');
title(['Comportamiento de la RNA para' , num2str(capas_oculta),' neuronas'])
xlabel('x')
ylabel('Y = x^3')
hold off
saveas(gcf,['comportamiento_rna_', num2str(capas_oculta),'capas.png'])
%%*********************************************************************
figure(2)
plot(entradas,salidas,'g');
grid on;
hold on;
plot(entradas,Y,'ro');
hold off
title(['Salida entrenamiento RNA para' , num2str(capas_oculta),' neuronas'])
xlabel('x')
ylabel('Y = x^3')
saveas(gcf,['salida_entrenamiento_rna_', num2str(capas_oculta),'capas.png'])
%%********************************************************************
figure(3)
plot(entradas_v,salidas_v,'g');
grid on;
hold on;
plot(entradas_v,V,'ro');
hold off
title(['Salida validacion RNA para' , num2str(capas_oculta),' neuronas'])
xlabel('x')
ylabel('Y = x^3')
saveas(gcf,['salida_validacion_rna_', num2str(capas_oculta),'capas.png'])
end